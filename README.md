## sys_oplus_mssi_64_cn-user 11 RP1A.200720.011 1612759478108 release-keys
- Manufacturer: alps
- Platform: mt6885
- Codename: oppo6889
- Brand: alps
- Flavor: sys_oplus_mssi_64_cn-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 1612759478108
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: alps/vnd_oppo6889/oppo6889:11/RP1A.200720.011/1612759478108:user/release-keys
- OTA version: 
- Branch: sys_oplus_mssi_64_cn-user-11-RP1A.200720.011-1612759478108-release-keys
- Repo: alps_oppo6889_dump_22974


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
